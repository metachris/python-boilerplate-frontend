# Python Boilerplate Web Frontend

[![build status](https://gitlab.com/metachris/python-boilerplate-frontend/badges/master/build.svg)](https://gitlab.com/metachris/python-boilerplate-frontend/commits/master)

Web frontend for [python-boilerplate.com](https://www.python-boilerplate.com)

Using

* [Vue.js](https://vuejs.org/)
* [vuex](https://vuex.vuejs.org/en/intro.html), vue-router
* [vue-highlightjs](https://github.com/metachris/vue-highlightjs)


## Getting started

Install the dependencies and start the dev server:

    # Install dependencies with yarn or npm
    $ yarn    # same as 'npm install'

    # run the dev server with hot reload at localhost:8080
    $ yarn run dev   # same as 'npm run dev'


## Interesting Source Files

Here is a list of source files to get you started:

* `src/main.js` - JavaScript entry point
* `src/settings.js` - environment-specific settings
* `src/App.vue` - Root Vue.js app
* `src/store/` - Vuex state store
* `src/routes/` - Root Vue.js components for various routes
* `src/components/` - Vue.js components


## Contributing

When you add new a boilerplate flag to the backend, this is how you add a new checkbox to the web frontend:

* Add the new config url slug to `src/store/constants.js`
* Add your new flag to the state store in `src/store/index.js`
  * Add a new `state` property
  * Add a mutation to set the state to true/false (for this, also define a mutation constant in `mutation-types.js`)
  * Update `getters`:
    * Create a getter for your new property
    * Update `getBoilerplateConfig(..)`
    * Update `getTitle(..)`
* In `src/store/actions.js`
  * Add an action to update the state
  * Update `generateBoilerplateByRoute(..)` to include the new config
* Finally, update the user interface in `src/components/Choices.vue`
  * Create a method which calls the action
  * Add your new getter to `mapGetters[..]`
  * Finally, add the HTML code for the control

See also: [How to add a new boilerplate](https://gitlab.com/metachris/python-boilerplate-templates/#contributing) in the template README.


## Releasing

* Commit the changes
* Upate HISTORY.md and commit
* Create a new version with `bumpversion major|minor|patch`
* Push with `git push && git push --tags`
